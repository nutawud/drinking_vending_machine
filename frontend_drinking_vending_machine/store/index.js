import Vuex from "vuex";
import axios from "axios";
import Swal from 'sweetalert2';

const createStore = () => {
  return new Vuex.Store({
    state: {
      location: [],
      product: [],

      responseBuyProduct: null,
      user: null
    },
    mutations: {
      setLocation(state, machines) {
        state.location = machines
      },
      setProduct(state, products) {
        state.product = products
      },
      setResBuyProduct(state, responseBuyProduct) {
        state.responseBuyProduct = responseBuyProduct
      },
      setUser(state, user) {
        state.user = user
      }
    },
    actions: {
      async getLocation(vuexCommit, context) {
        return await axios.get('http://localhost:3100/machine_companys').then(response => {
          if (response.status == 200) {
            vuexCommit.commit("setLocation", response.data)
          }
        }).catch(e => context.error(e));
      },

      async getProduct(vuexCommit, machine_id) {
        return await axios.get('http://localhost:3100/machine_company/' + machine_id)
          .then((response) => {
            if (response.status == 200) {
              vuexCommit.commit("setProduct", response.data)
            }
          })
          .catch((error) => console.log(error))
      },

      async buyProduct(vuexCommit, data) {
        let req = {
          quantity: data.quantity,
          name: data.name,
        }
        return await axios.put('http://localhost:3100/buy/product/' + data.id, req)
          .then((response) => {
            if (response.status == 200) {
              vuexCommit.commit("setResBuyProduct", response.data);
            }
          })
          .catch((error) => console.log(error))
      },

      async getUser(vuexCommit) {
        return await axios.get('http://localhost:3100/user')
          .then((response) => {
            if (response.status == 200) {
              vuexCommit.commit("setUser", response.data);
            }
          })
          .catch((error) => console.log(error))
      },

      async updateEmail(vuexCommit, data) {
        return await axios.put('http://localhost:3100/user/' + data.user_id, { email: data.email }).then(response => {
          if (response.status == 200) {
            Swal.fire({
              title: 'บันทึกสำเร็จ',
              icon: 'success',
              timer: 3000,
              showConfirmButton: false,
            })
          }
        }).catch(e => context.error(e));
      }
    },
    getters: {
      isAuthenticated(state) {
        return state.auth.loggedIn
      },

      loggedInUser(state) {
        return state.auth.user
      },

      // api
      getAllLocation(state) {
        return state.location
      },
      getAllProduct(state) {
        return state.product
      },
      resBuyProduct(state) {
        return state.responseBuyProduct
      },
      getUser(state) {
        return state.user
      }
    }
  })
}

export default createStore;