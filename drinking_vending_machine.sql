-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 23, 2021 at 07:07 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drinking_vending_machine`
--

-- --------------------------------------------------------

--
-- Table structure for table `machine_companys`
--

CREATE TABLE `machine_companys` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `machine_companys`
--

INSERT INTO `machine_companys` (`id`, `name`, `lat`, `lng`, `createdAt`, `updatedAt`) VALUES
(1, 'ตู้ 1', '13.8058793', '100.535343', '2021-09-22 07:53:12', '2021-09-22 07:53:12'),
(2, 'ตู้ 2', '13.7244416', '100.3529127', '2021-09-22 07:53:12', '2021-09-22 07:53:12');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `machine_company_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `createdAt` datetime NOT NULL,
  `updateAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `machine_company_id`, `name`, `quantity`, `price`, `createdAt`, `updateAt`) VALUES
(1, 1, 'เนสท์เล่ เพียวไลฟ์ น้ำดื่ม 1500มล. x 6 ขวด', 78, 20, '2021-09-20 11:00:00', '2021-09-20 10:00:00'),
(2, 1, 'โค้ก น้ำอัดลม รส ออริจินัล 325มล. 6 กระป๋อง', 12, 15, '2021-09-20 11:00:00', '2021-09-20 10:00:00'),
(3, 2, 'น้ำทิพย์ น้ำดื่ม 550มล. x 12 ขวด', 5, 15, '2021-09-20 11:00:00', '2021-09-20 10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `createdAt`, `updatedAt`) VALUES
(1, 'admin', 'nutawudtest@gmail.com', '$2a$08$k16Xiw/tMwUtzns5M5NAQ.x5Pi1vZP925/xxQeN1jDNaWK7EhaQeq', '2021-09-13 14:12:35', '2021-09-13 14:12:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `machine_companys`
--
ALTER TABLE `machine_companys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `machine_company_id` (`machine_company_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `machine_companys`
--
ALTER TABLE `machine_companys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
