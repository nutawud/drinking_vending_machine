const express = require('express'); // เรียกใช้ Express
const mysql = require('mysql'); // เรียกใช้ mysql
const bodyParser = require('body-parser');
const cors = require('cors');
const sgMail = require('@sendgrid/mail');

const db = mysql.createConnection({   // config ค่าการเชื่อมต่อฐานข้อมูล
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'drinking_vending_machine'
})
db.connect() // เชื่อมต่อฐานข้อมูล

const API_KEY = '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';
const app = express()
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.json('Wellcome')
});
// Select Data
app.get('/machine_companys', (req, res) => {
  let sql = 'SELECT * FROM machine_companys'
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
});

app.get('/machine_company/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM products WHERE machine_company_id = ?', [id], (err, results) => {
    if (err) throw err
    res.json(results)
  })
});

app.put('/buy/product/:id', (req, res) => {
  const product_id = req.params.id;
  const product_quantity = req.body.quantity;
  const product_name = req.body.name;
  const set_quantity = (product_quantity - 1);
  db.query('SELECT * FROM users WHERE username = ?', ['admin'], async (err, res_user) => {
    if (err) throw err
    db.query('UPDATE products SET quantity = ? WHERE id = ?', [set_quantity, product_id], (err, results) => {
      if (err) throw err

      if (results) {
        if (set_quantity < 10) {
          sgMail.setApiKey(API_KEY);
          const message = {
            to: res_user[0].email,
            from: 'nutawud110033@gmail.com',
            subject: 'แจ้งเตือนจาก Drinking Machine',
            text: 'Hello text',
            html: '<h1>' + product_name + '<br>เหลืออยู่ ' + set_quantity + ' ชิ้น</h1>',
          };
          sgMail.send(message)
            .then(res => { console.log('Send mail...') })
            .catch(err => { console.log(err) });
        }
        res.json({ status: 'success' })
      }
    });
  });

});

app.post('/auth', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  if (username && password) {
    db.query('SELECT * FROM users WHERE username = ?', [username], async (err, results) => {
      if (err) throw err
      res.json({
        data: {
          user: {
            username: results[0].username,
            email: results[0].username
          },
          token: 'TOEKN'
        }
      });
    });
  } else {
    res.send('Please enter Username and Password!');
    res.end();
  }
});

app.get('/user', (req, res) => {
  db.query('SELECT * FROM users WHERE username = ?', ['admin'], async (err, res_user) => {
    if (err) throw err
    res.json({
      data: res_user[0]
    })
  })
});

app.put('/user/:id', (req, res) => {
  const user_id = req.params.id;
  const email = req.body.email;

  if (email) {
    db.query('UPDATE users SET email = ? WHERE id = ?', [email, user_id], async (err, results) => {
      if (err) throw err
      res.json({
        status: 'success'
      })
    });
  }
  else {
    res.send('Please enter email!');
  }
});

app.listen('3100', () => {
  console.log('start port 3100')
})